
import { useState } from "react";


const nextId = (record) => {
    const len = record.length;
    if (len > 0) {
        var lastId = record[len - 1].id;
        return (lastId + 1)
    } else {
        return 0;
    }
}

export function CountryApp() {

    const [state, setState] = useState({
        countries: [
            { id: 1, countryname: 'India', code: '91', capital: 'Delhi', population: '10', area: '50' },
            { id: 2, countryname: 'Usa', code: '1', capital: 'abc', population: '15', area: '60' },
            { id: 3, countryname: 'Japan', code: '77', capital: 'Shicago', population: '20', area: '70' }
        ]
        , id: 0, countryname: '', code: '', capital: '', population: '', area: '' ,bLabel:'Add'
    })

    const deleteContry = (id) => {
        console.log('Delete :' + id)
        const temp = state.countries.filter((item) => (item.id != id));
        setState({ ...state, countries: temp });
    }

    const editContry = (id) => {
        console.log('Edit :' + id);
        const [record] = state.countries.filter((item) => (item.id == id))

        console.log(record);
        setState({
            ...state,
            ...record,
            bLabel:'Update'
        })
    }

    const Reload = () => {
        setState(...state,)
    }
    const HandleChange = (e) => {
        let key = e.target.name.toLowerCase();
        setState({ ...state, [key]: e.target.value })
    }

    const HandleSubmit = (e) => {
        e.preventDefault();
        console.log('Handle Submit');

        const newItem = {
            countryname: state.countryname,
            code: state.code,
            capital: state.capital,
            population: state.population,
            area: state.area,
            id: nextId(state.countries)
        }

        console.log('new :' + newItem);
        console.log('state :' + state.id);
        if (state.id === 0) {
            setState(prevState => ({
                countries: prevState.countries.concat(newItem),
                countryname: '',
                code: '',
                capital: '',
                population: '',
                area: '',
                id: 0,
                bLabel:'Add'
            }))

            console.log('Addded')
        } else {
            console.log('Update code :');
            newItem.id = state.id;
            const temp = state.countries.map((item) => {
                if (state.id == item.id) {
                    return newItem
                } else {
                    return item
                }
            })
            setState(prevState => ({
                countries: temp,
                countryname: '',
                code: '',
                capital: '',
                population: '',
                area: '',
                id: 0,
                bLabel:'Add'
            }))
        }
    }

    const ResetForm = (e) => {
        console.log('==Reset Called==')
        // e.preventDefault();
        setState(prevState => ({
            ...prevState,
            countryname: '',
            code: '',
            capital: '',
            population: '',
            area: '',
            id: 0,
            bLabel:'Add'
        }))
    }

    return (
        <div>
            <h2>Country App</h2>
            <form>
                <input
                    placeholder="Country Name"
                    name="countryname"
                    onChange={HandleChange}
                    value={state.countryname}
                /><br /><br />

                <input
                    placeholder="Country Code"
                    name="code"
                    onChange={HandleChange}
                    value={state.code}
                /><br /><br />

                <input
                    placeholder="Country Capital"
                    name="capital"
                    onChange={HandleChange}
                    value={state.capital}
                /><br /><br />

                <input
                    placeholder="Population"
                    name="population"
                    onChange={HandleChange}
                    value={state.population}
                /><br /><br />

                <input
                    placeholder="Country Area"
                    name="area"
                    onChange={HandleChange}
                    value={state.area}
                    id="area"
                /><br /><br />

                <input type="button"
                    value={state.bLabel}
                    onClick={HandleSubmit} />&nbsp;&nbsp;

                <input type="button"
                    onClick={ResetForm}
                    value="Reset" />
            </form>
            <CountryList countries={state.countries}
                editContry={editContry}
                deleteContry={deleteContry} />
        </div>
    )
}


function CountryList({ countries, editContry, deleteContry }) {
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CountryName</th>
                        <th>code</th>
                        <th>Capital</th>
                        <th>Population</th>
                        <th>Area</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {countries.map(country => (
                        <tr key={country.id}>
                            <td>{country.id}</td>
                            <td>{country.countryname}</td>
                            <td>{country.code}</td>
                            <td>{country.capital}</td>
                            <td>{country.population}</td>
                            <td>{country.area}</td>
                            <td><button onClick={() => editContry(country.id)}>Edit</button></td>
                            <td><button onClick={() => deleteContry(country.id)}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}