import logo from './logo.svg';
import './App.css';
import { CountryApp } from './Country';

function App() {
  return (
    <div className="App">
      <CountryApp/>
    </div>
  );
}

export default App;
